package comp2931.cwk1;

import org.junit.Test;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.*;

/**
 * Created by sc16rk on 06/11/17.
 */
public class DateTests {
  //Test to test the toString method
  @Test
  public void string() {
    // I create a new date, turn it into a string using the method then check the output is correct.
    assertThat(new Date(2017, 11 ,12 ).toString(), is("2017-11-12"));
    assertThat(new Date(1998, 8 ,24 ).toString(), is("1998-08-24"));
  }

  //Makes sure when an incorrect year is inputted an illegal argument is thrown
  @Test(expected=IllegalArgumentException.class)
  public void yearToolow() {
    new Date(-1, 11 ,2);
  }


  //Makes sure when an incorrect month is inputted an illegal argument is thrown
  @Test(expected=IllegalArgumentException.class)
  public void monthsToolow() {
    new Date(2017, 12 ,2);
  }




  //Makes sure when an incorrect day is inputted an illegal argument is thrown
  @Test(expected=IllegalArgumentException.class)
  public void dayswrong() {
    new Date(2017, 1 ,29);
    new Date(2017, 11 ,32);
  }

  //Tests if the equals method is working
  @Test
  public void equality(){
    Date today = new Date(2017, 1, 28);
    //check it matches cirrect dates
    assertTrue(today.equals(today));
    //check it notices dates that dont match
    assertFalse(today.equals(new Date(2017, 11 ,31 )));
  }

  //Tests the dayOfYear method works
  @Test
  public void getDayOfYear() {
    assertThat(new Date(2017, 1 ,28 ).getDayOfYear(), is(59));
    assertThat(new Date(2017, 10 ,5 ).getDayOfYear(), is(309));
    assertThat(new Date(2017, 11 ,31 ).getDayOfYear(), is(365));
    // leap year test checking if leap yaer works.
    //If it didnt work 2016/1/29 would throw an exception
    // 2017/2/1 proves that the month is permanently set to having 29 days.
    assertThat(new Date(2016, 1 ,29 ).getDayOfYear(), is(60));
    assertThat(new Date(2017, 2 ,1 ).getDayOfYear(), is(60));

  }
}