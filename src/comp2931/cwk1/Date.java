package comp2931.cwk1;

import java.util.Calendar;

/**
 * Created by sc16rk on 06/11/17.
 */

public class Date {
  //create array of days in each month January is array 0
  int[] monthArray = {31,28,31,30,31,30,31,31,30,31,30,31};
  private int year;
  private int month;
  private int day;

  /**
   * Creates a date using the given values for year, month and day.
   *
   * @param y Year
   * @param m Month
   * @param d Day
   */
  public Date(int y, int m, int d) {
    set(y ,m ,d);
    year = y;
    month = m;
    day = d;
  }

  /**
   * Returns the year component of this date.
   *
   * @return Year
   */
  public int getYear() {
    return year;
  }

  /**
   * Returns the month component of this date.
   *
   * @return Month
   */
  public int getMonth() {
    return month;
  }

  /**
   * Returns the day component of this date.
   *
   * @return Day
   */
  public int getDay() {
    return day;
  }

  /**
   * Provides a string representation of this date.
   *
   * ISO 8601 format is used (YYYY-MM-DD).
   *
   * @return Date as a string
   */
  @Override
  public String toString() {
    return String.format("%04d-%02d-%2d", year, month, day);
  }

  /**
   * Method checks that the user has inputted correct variables
   *
   * @return Dates
   */
  private void set(int y, int m, int d) {
    //Checks if the year is a leap year ( all leap years are divisible by 4 )
    if(y % 4 == 0){
      //change the month february to 29 days instead of 28
      monthArray[1] = 29;
    }
    //Checks no more then 12 months are inputted and no negatives are inputted
    if (m < 0 || m > 11) {
      throw new IllegalArgumentException("There are 12 months in a year");
    }

    //Make sure no negatives are inputted for years
    if (y < 0){
      throw new IllegalArgumentException("There are no negative years");
    }
    //Makes sure the day inputted is allways postive and is never more then the days in the chosen month
    if (d < 0 || d > monthArray[m]) {
      throw new IllegalArgumentException("There are 28,31 or 30 days in a month");
    }
    //If there is nothing wrong output them as they are.
    else {
      month = m;
      day = d;
      year = y;
    }
  }

  /**
   * Method checks if to dates are the same
   *
   * @return boolean
   */
  @Override
  public boolean equals(Object other) {
    if (other == this) {
      return true;
    }
    else if (! (other instanceof Date)) {
      // other is not a Date object
      return false;
    }
    else {
      // Compares the  fields
      Date otherDate = (Date) other;
      return getYear() == otherDate.getYear()
          && getMonth() == otherDate.getMonth()
          && getDay() == otherDate.getDay();
    }
  }

  /**
   * Method gets the day of the year, e.g 2017,01,20 is the 20th day of the year
   *
   * @return int
   */
  public int getDayOfYear() {
    int y = year;
    int m;
    int d;
    //Checks if the year is a leap year ( all leap years are divisible by 4 )
    if( y % 4 == 0 ){
      //If it is a leap year it set febuary to 29 days
      monthArray[1] = 29;
    }
    //Stores the total of days
    int total = 0;
    m = getMonth();
    d = getDay();
    if (m < 0) {
      throw new IllegalArgumentException("There are 12 months in a year");
    } else {
      while (m-1 >= 0){
        //month becomes the month before
        m = m-1;
        //Adds the month to total ( it will never add the month initally inputter)
        total = total + monthArray[m];
      }
    }
    //The output equals total + d ( this is because d is the number of days into the inital month)
    day = total + d;
    return day;
  }

  /**
   * Method gets the users current date using java calender class
   *
   * @return Date
   */
  public Date todaysDate(){
    //Gets the current date and save it as today
    Calendar today = Calendar.getInstance();
    int x = 0;
    int y = 0;
    int z = 0;
    //Set x,y and z and sets them as the current Year,month and day
    x = today.get(Calendar.YEAR);
    y = today.get(Calendar.MONTH);
    z = today.get(Calendar.DAY_OF_MONTH);
    //saves these variables as a new Date called todaysDate
    Date todaysDate = new Date(x, y, z);
    return todaysDate;
  }
}
